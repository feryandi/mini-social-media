###################
Langkah untuk melakukan Deploy 
###################
Diuji pada server menggunakan Nginx dan PHP

1. Import database dari dump yang telah tersedia
2. Copy isi folder src ke root web server tersebut (bisa juga di dalam folder)
3. Ubah pengaturan pada application/config/config.php pada base_url untuk disesuaikan dengan url yang baru
   Contoh: pada server lokal, biasanya digunakan http://localhost/
4. Ubah pengaturan pada application/config/database.php untuk disesuaikan dengan pengaturan database server
5. Ubah akses permission dari folder hasil copy src tersebut menjadi www:www [ chown -R www:www * ]
   Hal ini dilakukan agar server dapat membuat file session baru dan menyimpannya
6. Akses web melalui browser dengan URL yang sesuai

###################
Link Halaman
###################
Halaman depan: http://feryand.in/

Halaman login admin dan user: http://feryand.in/login

Login admin - user: admin; pass: default


###################
Fitur
###################

* Registrasi User
* Login User
* Kustomisasi Profile
* Status Update
* Timeline
* Menambah Teman

*Admin Only*

* Info User lengkap pada satu halaman (+last login)
* Menambahkan / mengurangi User baru
* CRUD terhadap seluruh User yang terdaftar
* Menambahkan / mengurangi Admin

*Developer*

* API