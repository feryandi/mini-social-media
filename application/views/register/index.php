<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php 
			if ( empty($title) ) {
				echo "Register - Mini Social Media";
			} else {
				echo $title;
			}
		?></title>
		<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	</head>

	<body class="no-padding">
		<div class="container full-height">
			<div class="logo-welcome">
		       	<a href="<?php echo base_url(); ?>">
			        <span class="huge-logo">MSM</span>
			        <p class="lead logo-slogan">a simple social media for your daily life</p>
			    </a>
			</div>

			<div class="row">
		        <div class="col-md-offset-3 col-md-6 col-xs-12">
			      <div class="modal-body">
					<div class="row">
						<div class="col-md-offset-2 col-md-8 col-xs-12">
							<div class="row row-flex">
								<div class="col-md-2 col-xs-2">
									<hr/>
								</div>
								<div class="col-md-8 col-xs-8 or-signup">
									<i>Join Us and Get Connected</i>
								</div>
								<div class="col-md-2 col-xs-2">
									<hr/>
								</div>
							</div>
							
							<?php if ( validation_errors() ) { ?>
								<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
							<?php } ?>

					        <?php echo form_open('/register/send'); ?>
								<div class="form-group">
						    		<input class="form-control" name="username" id="username" placeholder="Username" maxlength="25" value="<?php echo set_value('username'); ?>" required title="Fill this">
								</div>
								<div class="form-group">
						    		<input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>" required>
								</div>		
								<div class="form-group">
						    		<input class="form-control" name="email" id="email" placeholder="E-mail" value="<?php echo set_value('email'); ?>" required>
								</div>

								<div class="form-group">
									<div class="row row-flex input-group name-fixer">
						    			<input class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo set_value('first_name'); ?>" style="width: 50%" required>
						    			<input class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo set_value('last_name'); ?>" style="width: 50%" required>
						    		</div>
					    		</div>

								<div class="form-group">
						    		<input class="form-control" name="relationship" id="relationship" placeholder="Relationship" value="<?php echo set_value('relationship'); ?>" required>
								</div>

								<div class="row">
									<div class="col-md-offset-7 col-md-5 col-xs-offset-6 col-xs-6">
										<button type="submit" class="btn btn-success btn-block">Register</button>
									</div>
								</div>

							</form>

						</div>
					</div>

			      </div>
			</div>

		</div>

	</body>

</html>