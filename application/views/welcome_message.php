<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php 
			if ( empty($title) ) {
				echo "Mini Social Media";
			} else {
				echo $title;
			}
		?></title>
		<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	</head>

	<body class="no-padding">
		<div class="container">
			<div class="logo-welcome">
		       	<a href="">
			        <span class="huge-logo">MSM</span>
			        <p class="lead logo-slogan">a simple social media for your daily life</p>
			    </a>
			</div>

			<div class="row">
		        <div class="col-md-offset-3 col-md-6 col-xs-12 col-sm-12">
		        <?php echo form_open('/login/auth'); ?>
			      <div class="modal-body">
					<div class="row">
						<div class="col-md-offset-2 col-md-8 col-xs-12">								

							<div class="form-group">
					    		<input class="form-control" name="username" id="username" placeholder="Username" maxlength="25" required>
							</div>
							<div class="form-group">
					    		<input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
							</div>	

							<div class="row row-flex">
								<div class="col-md-8 col-xs-8 margin-auto">
									<a href="#">Forgot your password?</a>
								</div>
								<div class="col-md-4 col-xs-4">
									<button type="submit" class="btn btn-primary btn-block">Sign In</button>
								</div>							
							</div>

						</div>
					</div>

					<div class="row row-flex">
						<div class="col-md-5 col-xs-5">
							<hr/>
						</div>
						<div class="col-md-2 col-xs-2 or-signup">
							<i>or</i>
						</div>
						<div class="col-md-5 col-xs-5">
							<hr/>
						</div>
					</div>

					<div class="row">
						<div class="col-md-offset-2 col-md-8 col-xs-12">
							<a href="register" class="btn btn-success btn-block">Sign Up for FREE</a>
						</div>
						</div>
					</div>


			      </div>
				</form>
			</div>

		</div>

	</body>

	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
</html>