	<div class="row">
		<div class="col-md-8">
			
			<div class="box timeline_status no-margin">
				<div class="row no-margin">
					<div class="col-md-2 col-xs-0 col-sm-2">
						<div class="text-center profile_picture hidden-when-small">
							<?php 
								if ( empty($photo) ) {
									echo '<img src="img/default.png" />';
								} else {
									echo '<img src="'. base_url() .'' . $photo . '" />';									
								}
							?>
						</div>
					</div>
					<div class="col-md-10 col-xs-12 col-sm-10">
					    
					    <?php echo form_open('/home/add_status'); ?>
							<div class="row row-flex ">
								<div class="col-md-10 col-xs-9">
									<div class="content">
										
										<div class="form-group no-margin">
											<textarea name="status" class="form-control" rows="3"></textarea>
										</div>
										
									</div>
								</div>
								<div class="col-md-2 col-xs-3 margin-auto no-padding text-center">
									<div class="form-group no-margin text-right">
										<button class="btn btn-primary btn-block" onclick="changeLineBreak()" type="submit">Share</button>
									</div>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>

			<div class="row row-flex no-margin">
				<div class="col-md-12 col-xs-12 center light-gray">
					
					<h4 class="font-baron color-white">
						Latest Update from Your Friends
					</h4>

				</div>
			</div>

			<?php 
				if ( !empty($timeline) ) {
					foreach ($timeline as $status) {
			?>
			
			<div class="box timeline_status no-margin">
				<div class="row row-flex no-margin">
					<div class="col-md-2">
						<div class="text-center profile_picture">
							<?php 
								if ( empty($status->photo ) ) {
									echo '<img src="img/default.png" />';
								} else {
									echo '<img src="'. base_url() .'' . $status->photo . '" />';									
								}
							?>
						</div>
					</div>
					<div class="col-md-10">
						<?php if($status->profile_id == $id) { ?>
						<a type="button" class="close" href="profile/delete_status/<?php echo $status->id; ?>" onclick="return confirm('Are you sure want to delete this status?')"><span aria-hidden="true">&times;</span></a>
						<?php } ?>
						<div class="name">
							<a href="profile/show/<?php echo $status->profile_id; ?>"><?php echo html_escape($status->first_name) . " " . html_escape($status->last_name); ?></a>
						</div>
						<div class="timestamp">
							<?php echo date("d F Y - H:i", strtotime($status->date)); ?>
						</div>
						<div class="content">
							<?php 
								echo str_replace("\n", "<br \>", html_escape($status->content));  ?>
						</div>
					</div>
				</div>
			</div>

			<?php
					}
				} else {
			?>

			<div class="box timeline_status no-margin">
				<div class="row">
					<center>No latest status update <br> Add Friends and Get Connected!</center>
				</div>
			</div>

			<?php
				}
			?>

		</div>

		<div class="col-md-4">

			<div class="affix" style="width: 22%">
				<div class="row row-flex no-margin hidden-when-small">
					<div class="col-md-12 col-xs-12 center light-gray">
						
						<h4 class="font-baron color-white">
							<span id="greetings" > </span>,
							<?php echo html_escape($first_name); ?>
						</h4>

					</div>
				</div>

				<div class="box timeline_status no-margin no-padding hidden-when-small">
					<div class="row margin-auto">

						<img id="greetings-illust" src="" width="100%">

					</div>
				</div>
			</div>

		</div>

		<script src="<?php echo base_url(); ?>js/greetings.js"></script>
	</div>
