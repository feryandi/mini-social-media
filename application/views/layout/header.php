<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php 
			if ( empty($title) ) {
				echo "Mini Social Media";
			} else {
				echo $title;
			}
		?></title>
		<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />	
	</head>

	<body>
	
		<nav class="navbar navbar-default navbar-fixed-top">

	     	<div class="container">

		    	<div class="row">
		    		<div class="col-md-offset-1 col-md-10 col-xs-12">
				        <div class="navbar-header">
				          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				          <a class="navbar-brand tiny-logo" href="<?php echo base_url(); ?>">MSM</a>
				        </div>
				        <div id="navbar" class="collapse navbar-collapse">

				          <ul class="nav navbar-nav">
				            <li><a href="<?php echo base_url(); ?>home">Timeline</a></li>
				            <li><a href="<?php echo base_url(); ?>profile">Profile</a></li>
				            <li><a href="<?php echo base_url(); ?>home/logout">Logout</a></li>
				          </ul>
				          <form class="navbar-form navbar-right" method="get" action="<?php echo base_url(); ?>search">
			            	<div class="input-group">
			              		<input name="q" id="q" placeholder="search..." class="form-control">
			            		<span class="input-group-btn">
				            		<button type="submit" class="btn btn-default">Search</button>
				            	</span>
				            </div>
				          </form>

				        </div><!--/.nav-collapse -->
					</div>
				</div>
			</div>

	      	</div>
         </div>

	    </nav>

		<div class="container" style="min-height: calc(100vh - 110px);">
			<div class="row">				
				<div class="col-md-offset-1 col-md-10 col-xs-12">
