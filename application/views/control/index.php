
<nav>
  <ul class="pager">
  	<?php
  		if ( $data['page_id'] - 1 >= 0 ) {
  	?>
	    <li class="previous"><a href="<?php echo base_url(); ?>control/<?php echo $data['page_id'] - 1; ?>"><span aria-hidden="true">&larr;</span> Previous</a></li>
	<?php
		}

		if ( ( $data['page_id'] + 1 ) * $data['user_per_page'] < $data['total_users'] ) { 
	?>
    <li class="next"><a href="<?php echo base_url(); ?>control/<?php echo $data['page_id'] + 1; ?>">Next <span aria-hidden="true">&rarr;</span></a></li>
  	<?php
  		}
  	?>
  </ul>
</nav>

<?php
	if ( !is_null($data[0]) ) {
		foreach ($data[0] as $d) {
?>
		<div class="box basic_info no-margin search_result">
			<div class="container">

				<div class="row no-margin">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<div class="text-center profile_picture little">
							<?php 
								if ( $d->photo == NULL ) {
									echo '<img src="' . base_url() . 'img/default.png" />';
								} else {
									echo '<img src="' . base_url() . '' . $d->photo . '" />';									
								}
							?>
						</div>
					</div>

					<div class="col-md-8 col-sm-8 col-xs-12 margin-auto search">
						<?php
							echo "<div class='name'>" . html_escape($d->first_name) . " " . html_escape($d->last_name)  . "</div>";
							echo "<span class='username'> (" . $d->username . ") </span>";
							echo "<div class='email'>" . $d->email . "</div>";
							echo "<div class='email'>" . $d->relationship_status . "</div>";

							echo "<div class='email'>";
								if ( $d->job != NULL ) {
									echo $d->job; 
								}
							echo "</div>";

							echo "<div class='email'>";
								if ( $d->place_of_birth != NULL ) {
									echo $d->place_of_birth;
								}
								if ( strtotime($d->date_of_birth) ) {
									echo " " . date("d M Y", strtotime($d->date_of_birth));
								}
							echo "</div>";

							echo "<div class='email'>Last login " . $d->last_login . "</div>";
						?>
					</div>

					<div class="col-md-2 col-sm-2 col-xs-12 text-center mobile-padding auto-margin">
						<a type="button" class="btn btn-default pc-button" href="<?php echo base_url(); ?>control/edit_user/<?php echo $d->id; ?>">Edit Profile</a>
						<a type="button" class="btn btn-default pc-button" href="<?php echo base_url(); ?>control/edit_picture/<?php echo $d->id; ?>">Edit Photo</a>
						<a type="button" class="btn btn-danger pc-button" href="<?php echo base_url(); ?>control/delete_user/<?php echo $d->id; ?>" onclick="return confirm('Are you sure want to delete this user?')">Delete</a>
					</div>
				</div>

			</div>	
		</div>

<?php
	}
?>

		<nav>
		  <ul class="pager">
		  	<?php
		  		if ( $data['page_id'] - 1 >= 0 ) {
		  	?>
			    <li class="previous"><a href="<?php echo base_url(); ?>control/<?php echo $data['page_id'] - 1; ?>"><span aria-hidden="true">&larr;</span> Previous</a></li>
			<?php
				}

				if ( ( $data['page_id'] + 1 ) * $data['user_per_page'] < $data['total_users'] ) { 
			?>
		    <li class="next"><a href="<?php echo base_url(); ?>control/<?php echo $data['page_id'] + 1; ?>">Next <span aria-hidden="true">&rarr;</span></a></li>
		  	<?php
		  		}
		  	?>
		  </ul>
		</nav>

<?php
} else {
?>

	<div class="box timeline_status">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					No Search Result
				</div>
			</div>
		</div>
	</div>

<?php
}
?>