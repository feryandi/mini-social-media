<div class="row no-margin">
	<div class="col-md-12 col-xs-12 center light-gray">
		<div class="row row-flex no-margin">
			<div class="col-md-4 col-xs-4">
				<div class="text-center profile_picture little">
					<?php 
						if ( $photo == NULL ) {
							echo '<img src="' . base_url() . 'img/default.png" />';
						} else {
							echo '<img src="' . base_url() . '' . $photo . '" />';									
						}
					?>
				</div>
			</div>

			<div class="col-md-8 col-xs-8 margin-auto">
				<center><span class="profile_name"><?php echo html_escape($first_name) . " " . html_escape($last_name); ?></span></center>
			</div>
		</div>
	</div>
</div>

<form method="post" accept-charset="utf-8" action="<?php echo base_url(); ?>control/send_edit/<?php echo $id; ?>">
	<div class="row box basic_info no-margin">

		<?php if ( validation_errors() ) { ?>
			<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
		<?php } ?>
		<div class="col-xs-12 col-md-6">
			<h3>Your Primary Profile</h3>
			<hr>

			<div class="form-group">
				<label for="exampleInputEmail1">Password</label>
	    		<input type="password" class="form-control" name="password" id="password" placeholder="Password Unchanged" value="">
			</div>		
			<div class="form-group">
				<label for="exampleInputEmail1">E-mail</label>
	    		<input class="form-control" name="email" id="email" placeholder="E-mail" required value="<?php echo $email; ?>">
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">First Name</label>
	    		<input class="form-control" name="first_name" id="first_name" placeholder="First Name" required value="<?php echo $first_name; ?>">
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Last Name</label>
	    		<input class="form-control" name="last_name" id="last_name" placeholder="Last Name" required value="<?php echo $last_name; ?>">
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Relationship</label>
	    		<input class="form-control" name="relationship" id="relationship" placeholder="Relationship" required value="<?php echo $relationship_status; ?>">
			</div>
		</div>

		<div class="col-xs-12 col-md-6">
			<h3>Additional Info</h3>
			<hr>

			<div class="form-group">
				<label for="exampleInputEmail1">Birth Place</label>
	    		<input class="form-control" name="birth_place" id="birth_place" placeholder="Birth Place" value="<?php echo $place_of_birth; ?>">
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Birthday</label>
				<?php $time = strtotime($date_of_birth); ?>
				<?php 
					$dob = date("d", $time); 
					$mob = date("m", $time); 
					$yob = date("Y", $time); 
				?>
				<div class="input-group row-flex">
		    		<input type="number" class="form-control" name="birth_date" id="birth_date" placeholder="D" min="1" max="31" value="<?php echo $dob; ?>">
		    		<input type="number" class="form-control" name="birth_month" id="birth_month" placeholder="M" min="1" max="12" value="<?php echo $mob; ?>">
		    		<input type="number" class="form-control" name="birth_year" id="birth_year" placeholder="Y" min="<?php echo ( date("Y") - 100 ); ?>" max="<?php echo date("Y"); ?>" value="<?php echo $yob; ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Occupancy</label>
	    		<input class="form-control" name="occupancy" id="occupancy" placeholder="Occupancy" value="<?php echo $job; ?>">
			</div> 
			<div class="form-group">
				<label for="exampleInputEmail1">Admin</label>
				<div class="radio">
		    		<label class="radio-inline"><input type="radio" name="is_admin" value="1" <?php if ($is_admin) { echo "checked"; } ?>>Yes</label>
					<label class="radio-inline"><input type="radio" name="is_admin" value="0" <?php if (!$is_admin) { echo "checked"; } ?>>No</label>
				</div>
			</div> 
		</div>
	
		<div class="col-xs-12 col-md-12">
			<button type="submit" class="btn btn-primary">Edit Profile</button>
		</div>
	
	</div>
</form>