
<div class="row box basic_info no-margin light-gray">
	<center><span class="profile_name">Add New User</span></center>
</div>

<div class="row">		
	<div class="col-md-12">
		<form method="post" accept-charset="utf-8" action="<?php echo base_url(); ?>control/new_user" />
			<div class="row box basic_info no-margin search_result">
				<div class="col-md-offset-4 col-md-4">
					<?php if ( validation_errors() ) { ?>
						<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
					<?php } ?>
					<div class="form-group">
			    		<input class="form-control" name="username" id="username" placeholder="Username" maxlength="25" required>
					</div>
					<div class="form-group">
			    		<input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
					</div>		
					<div class="form-group">
			    		<input class="form-control" name="email" id="email" placeholder="E-mail" required>
					</div>

					<div class="form-group">
						<div class="row row-flex input-group name-fixer">
			    			<input class="form-control" name="first_name" id="first_name" placeholder="First Name" required>
			    			<input class="form-control" name="last_name" id="last_name" placeholder="Last Name" required>
			    		</div>
		    		</div>

					<div class="form-group">
			    		<input class="form-control" name="relationship" id="relationship" placeholder="Relationship" required>
					</div>

					<div class="row">
						<div class="col-md-offset-7 col-md-5 col-xs-offset-6 col-xs-6">
							<button type="submit" class="btn btn-success btn-block">Register</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>