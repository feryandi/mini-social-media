
<div class="row no-margin">
	<div class="col-md-12 col-xs-12 center light-gray">
		<div class="row row-flex-md no-margin">
			<div class="col-md-4 col-xs-12">
				<div class="text-center profile_picture little">
					<?php 
						if ( $photo == NULL ) {
							echo '<img src="'. base_url() .'img/default.png" />';
						} else {
							echo '<img src="'. base_url() .'' . $photo . '" />';									
						}
					?>
				</div>
			</div>

			<div class="col-md-8 col-xs-12 margin-auto">
				<center><span class="profile_name"><?php echo html_escape($first_name) . " " . html_escape($last_name); ?></span></center>
			</div>
		</div>
	</div>
</div>

<form method="post" accept-charset="utf-8" action="<?php echo base_url(); ?>profile/send_profile_picture" enctype="multipart/form-data">
	<div class="row box basic_info no-margin">

		<div class="col-md-12 col-xs-12">
			<h3>Change Profile Picture</h3>
			<hr>

			<div class="form-group">			    		
	    		<input type="file" class="form-control" id="picture" name="picture" required>
			</div>		

			<button type="submit" class="btn btn-primary">Change</button>		
		</div>
		
	</div>
</form>
