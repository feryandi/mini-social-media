
	<div class="row no-margin">
		<div class="col-md-12 col-xs-12 center light-gray">
			<div class="text-center profile_picture">
				<?php 
					if ( $photo == NULL ) {
						echo '<img src="'. base_url() .'img/default.png" />';
					} else {
						echo '<img src="' . base_url() . '' . $photo . '" />';									
					}
				?>
			</div>
			<h1 class="profile_name"><?php echo html_escape($first_name) . " " . html_escape($last_name); ?></h1>
			
			<?php 
				if ( $dob = strtotime($date_of_birth) ) {
					if ( date("Y", $dob) > 1900 ) {
			?>
			<h4 class="profile_birthday">
				<img src="<?php echo base_url(); ?>img/icon_birthday_white.png" width="25px">
				<span class="bday_date">
					<?php 		
							echo date("d M Y", $dob);
					?>
				</span>
			</h4>
			<?php
					}
				}
			?>

		</div>
	</div>

	<div class="row box basic_info no-margin">
		<?php 
			if ( $id == $session_userid ) {
		?>
		<div class="col-md-6 col-xs-12 bacic_info_row">
		<?php
			} else {
		?> 
		<div class="col-md-12 col-xs-12 bacic_info_row">
		<?php
			}
		?>
			
			<div class="row text-center">
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_element">E-mail</span>
				</div>
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_element">Username</span>
				</div>
			</div>

			<div class="row text-center">
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_value"><?php echo $email ?></span>										
				</div>
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_value"><?php echo html_escape($username) ?></span>										
				</div>
			</div>
			
		</div>

		<?php 
			if ( $id == $session_userid ) {
		?>
		<div class="col-md-6 col-xs-12 bacic_info_row">
			
			<div class="row text-center">
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_element">Relationship</span>
				</div>
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_element">Occupancy</span>
				</div>
			</div>

			<div class="row text-center">
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_value"><?php echo html_escape($relationship_status) ?></span>										
				</div>
				<div class="col-md-6 col-xs-6">
					<span class="basic_info_value"><?php echo html_escape($job) ?></span>										
				</div>
			</div>
			
		</div>
		<?php
			}
		?>
	</div>	

	<div class="row profile_buttons">
		<div class="col-md-12 col-xs-12 text-center">

			<?php 
				if ( $id == $session_userid ) {
			?>
				<a href="profile/edit" class="btn btn-default">Edit Profile</a>
				<a href="profile/edit_picture" class="btn btn-default">Change Picture</a>
			<?php
				} else {
					if ( !$this->FriendModel->is_friend($id, $session_userid) ) {
			?>
				<?php echo form_open('/profile/add_friend'); ?>
					<input type="hidden" name="csrf_key" value="<?php echo md5($id . $this->config->config['encryption_key']); ?>">
					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<button type="submit" class="btn btn-default">Add as Friend</button>
				</form>
			<?php
					} else {
						echo "You're already friend with " . html_escape($first_name);
					}
				}
			?>

		</div>
	</div>

	<div class="row">
		<div class="col-md-4 col-xs-12 text-center">
			<div class="row no-margin">
				<div class="col-md-12 col-xs-12 center light-gray">
					
					<h4 class="font-baron color-white">Friends</h4>

				</div>
			</div>

			<div class="box timeline_status no-margin">
				<div class="row">
					<?php
						$friends = $this->FriendModel->get_some_friend_photo($id);
						if ( count($friends) > 1 ) {
							foreach( $friends as $friend_photo ) {
								if ( $friend_photo->id != $id ) {
					?>
					<div class="col-md-4 col-xs-3 col-sm-2 no-padding">
						<div class="text-center profile_picture">
								<a href="<?php echo base_url(); ?>profile/show/<?php echo $friend_photo->id; ?>">
							<?php
									if ( $friend_photo->photo == NULL ) {
										echo '<img src="'. base_url() .'img/default.png" />';
									} else {
										echo '<img src="' . base_url() . '' . $friend_photo->photo . '" />';									
									}
							?>
								</a>
						</div>
					</div>
					<?php
								}
							}
						} else {
					?>
						<div class="content">
							<?php if ($id == $session_userid) { ?>
							You have no connections :( <br/> <br/> Start making one by  <br/> searching someone's name!
							<?php } else { ?>
							He/She have no connections :( <br/> <br/> Help him/her by adding him/her as your friend!						
							<?php } ?>
						</div>
					<?php 
						}
					?>
				</div>
			</div>

		</div>

		<div class="col-md-8 col-xs-12">

			<div class="row no-margin">
				<div class="col-md-12 col-xs-12 center light-gray">
					
					<h4 class="font-baron color-white text-center">Timeline</h4>

				</div>
			</div>

			<?php 
				if ( !empty($statuses) ) {
					foreach ($statuses as $status) {
			?>
			<div class="box timeline_status no-margin">
				<div class="row row-flex no-margin">
					<div class="col-md-2 hidden-when-tiny">
						<div class="text-center profile_picture">
							<?php 
								if ( $photo == NULL ) {
									echo '<img src="'. base_url() .'img/default.png" />';
								} else {
									echo '<img src="' . base_url() . '' . $photo . '" />';									
								}
							?>
						</div>
					</div>
					<div class="col-md-10 col-xs-12">
						<?php if($status->profile_id == $session_userid) { ?>
						<a type="button" class="close" href="profile/delete_status/<?php echo $status->id; ?>" onclick="return confirm('Are you sure want to delete this status?')"><span aria-hidden="true">&times;</span></a>
						<?php } ?>
						<div class="name">
							<?php echo html_escape($first_name) . " " . html_escape($last_name); //TO-DO: Fix this if more function implemented! ?> 
						</div>
						<div class="timestamp">
							<?php echo date("d F Y - H:i", strtotime($status->date)); ?>
						</div>
						<div class="content">
							<?php 
								echo str_replace("\n", "<br \>", html_escape($status->content)); 
							?>
						</div>
					</div>
				</div>
			</div>
			<?php
					}
				} else {
			?>


			<div class="box timeline_status no-margin">
				<div class="row row-flex no-margin">
					<div class="col-md-12">
						<div class="content">
							No activity yet....
						</div>
					</div>
				</div>
			</div>

			<?php
				}
			?>
		</div>
	</div>	

