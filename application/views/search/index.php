
<div class="row box basic_info no-margin light-gray">
	<center><span class="profile_name">Search Result</span></center>
</div>


<div class="row no-margin">
<?php
	if ( !is_null($data[0]) ) {
		foreach ($data[0] as $d) {
?>
	<a href="profile/show/<?php echo $d->id; ?>">

			<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
				<div class="box basic_info no-margin search_result">
					<div class="container">
						<div class="row no-margin">
							<div class="col-md-4 col-xs-12">
								<div class="text-center profile_picture little">
									<?php 
										if ( $d->photo == NULL ) {
											echo '<img src="img/default.png" />';
										} else {
											echo '<img src="' . base_url() . '' . $d->photo . '" />';									
										}
									?>
								</div>
							</div>

							<div class="col-md-8 col-sm-8 col-xs-12 margin-auto search">
								<div class="name">
								<?php 
									echo html_escape($d->first_name) . " " . html_escape($d->last_name);
								?>
								</div>
								<div class="username">
								<?php 
									echo "( " . html_escape($d->username) . " )";
								?>
								</div>
								<div class="email">
								<?php 
									echo html_escape($d->email);
								?>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>

	</a>	

<?php
		}
?>
</div>
<?php
	} else {
?>

		<div class="box basic_info no-margin timeline_status">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						No Result
					</div>
				</div>
			</div>
		</div>

<?php
	}
?>


