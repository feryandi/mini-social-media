<?php
    class AuthModel extends CI_Model {

        var $username = '';
        var $password = '';

        function __construct() {
            // Call the Model constructor
            parent::__construct();
        }

        function user_login() {
            $this->username   = $this->input->post('username', TRUE);
            $this->password = hash('sha512', $this->input->post('password', TRUE));

            $this->db->from('users');
            $this->db->where('username', $this->username);
            $this->db->where('password', $this->password);
            $query = $this->db->get();

            $this->db->flush_cache();

            $data = array(
                        'last_login' => date("Y-m-d H:i:s", time())
                    );
            $this->db->where('username', $this->username);
            $this->db->update('users', $data); 

            if ( $query->result() == NULL ) {

                return NULL;

            } else {

                return $query->result();

            }
        }

    }