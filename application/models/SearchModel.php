<?php
    class SearchModel extends CI_Model {

        public function __construct() {
            // Call the Model constructor
            parent::__construct();
        }

        public function user($query) {
            
            $this->db->select('id, first_name, last_name, username, email, photo');
            $this->db->from('users');
            $this->db->like('first_name', $query);
            $this->db->or_like('last_name', $query); 
            $this->db->or_like('username', $query); 
            $this->db->or_like('email', $query); 

            $data = $this->db->get();
            if ( $data->result() != NULL ) {

                return ($data->result());

            } else {

                return NULL;

            }

        }

    }