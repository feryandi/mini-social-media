<?php
    class FriendModel extends CI_Model {

        public function __construct() {
            // Call the Model constructor
            parent::__construct();
        }

        public function add_friend($profile_id, $target_id) {
            date_default_timezone_set('Asia/Jakarta');
            $data = array(
                        'requester_id' => $profile_id,
                        'accepter_id' => $target_id,
                        'date' => date("Y-m-d H:i:s", time())
                    );

            $this->db->insert('users_friends', $data);
            
        }

        public function is_friend($profile_id, $target_id) {   
            $this->db->from('users_friends');
            $this->db->where("( requester_id = $target_id AND accepter_id = $profile_id )");   
            $this->db->or_where("( requester_id = $profile_id AND accepter_id = $target_id )");  

            $query = $this->db->get();

            if ( $query->result() != NULL ) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public function get_all_friend_id($profile_id) {    

            $this->db->select('requester_id as id');  
            $this->db->from('users_friends');
            $this->db->where('accepter_id', $profile_id);   

            $query = $this->db->get();
            $data1 = $query->result();  

            $this->db->flush_cache();

            $this->db->select('accepter_id as id');  
            $this->db->from('users_friends');
            $this->db->where('requester_id', $profile_id); 

            $query = $this->db->get();
            $data2 = $query->result();  

            return array_merge($data1, $data2);
        }

        public function get_all_friend_photo($profile_id) {    
            $arr = $this->get_all_friend_id($profile_id);
            $this->db->flush_cache();

            $this->db->select("id, photo");
            $this->db->from("users");

            $this->db->where("id", $profile_id);

            foreach ( $arr as $friend) {
                $this->db->or_where("id", $friend->id);
            }

            $query = $this->db->get();

            return $query->result();
        }

        public function get_some_friend_photo($profile_id) {    
            $arr = $this->get_all_friend_id($profile_id);
            $this->db->flush_cache();

            $this->db->select("id, photo");
            $this->db->from("users");

            $this->db->where("id", $profile_id);

            foreach ( $arr as $friend) {
                $this->db->or_where("id", $friend->id);
            }

            $this->db->limit(15);

            $query = $this->db->get();

            return $query->result();
        }

    }