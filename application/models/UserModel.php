<?php
    class UserModel extends CI_Model {

        var $username = '';
        var $password = '';
        var $email = '';
        var $first_name = '';
        var $last_name = '';
        var $relationship_status = '';
        var $job = '';
        var $place_of_birth = '';
        var $date_of_birth = '';

        public function __construct() {
            // Call the Model constructor
            parent::__construct();
        }

        public function insert_new_user() {
            $this->username   = html_escape($this->input->post('username', TRUE));
            $this->password = hash('sha512', $this->input->post('password'));
            $this->email = $this->input->post('email', TRUE);
            $this->first_name = html_escape($this->input->post('first_name', TRUE));
            $this->last_name = html_escape($this->input->post('last_name', TRUE));
            $this->relationship_status = html_escape($this->input->post('relationship', TRUE));

            $this->db->from('users');
            $this->db->where('username', $this->username);
            $this->db->or_where('email', $this->email);

            $query = $this->db->get();
            if ( $query->result() == NULL ) {

                $this->db->insert('users', $this);
                return $this->username;

            } else {

                return NULL;

            }

        }

        public function user_profile($id) {

            $this->db->from('users');
            $this->db->where('id', $id);
            $query = $this->db->get();
            if ( $query->result() == NULL ) {

                redirect('login', 'refresh');

            } else {

                return $query->result();

            }

        }

        public function edit_profile($id) {
            $data = array();

            /* Primary Info */            
            if ( $temppass = $this->input->post('password') ) {
                $data['password'] = hash('sha512', $temppass);
            }

            $data['email'] = html_escape($this->input->post('email', TRUE));
            $data['first_name'] = html_escape($this->input->post('first_name', TRUE));
            $data['last_name'] = html_escape($this->input->post('last_name', TRUE));
            $data['relationship_status'] = html_escape($this->input->post('relationship', TRUE));
            
            /* Additional Info */
            $data['job'] = html_escape($this->input->post('occupancy', TRUE));
            $data['place_of_birth'] = html_escape($this->input->post('birth_place', TRUE));

            /* Admin !! */
            $data['is_admin'] = html_escape($this->input->post('is_admin', TRUE));

            $dob = strtotime($this->input->post('birth_year') . "-" . $this->input->post('birth_month') . "-" . $this->input->post('birth_date'));
            $data['date_of_birth'] = date("Y-m-d", $dob);

            //$this->date = time();

            $this->db->update('users', $data, array('id' => $id));
    
        }

        public function edit_profile_picture($id, $data) {

            //$this->date = time();
            $update = array();
            $update['photo'] = "/uploads/" . $data['file_name'];

            $this->db->update('users', $update, array('id' => $id));
    
        }

        public function get_all_users() {
            $query = $this->db->get('users');
            return $query->result();
        }

        public function get_some_users($limit, $cons) {

            $query = $this->db->get('users', $cons, ($limit * $cons));
            return $query->result();
        }

        public function get_total_users() {
            return $this->db->count_all('users');
        }

        public function delete_user($id) {
            $this->db->delete('users', array('id' => $id)); 
        }

        public function is_admin($id) {

            $this->db->from('users');
            $this->db->where('id', $id);
            $this->db->where('is_admin', 1);

            $query = $this->db->get();
            if ( $query->result() == NULL ) {

                return FALSE;

            } else {

                return TRUE;

            }

        }


        /* ************ API ************ */

        public function api_get_user_profile_by_username($username) {

            $this->db->select('id, username, first_name, last_name, email, relationship_status, place_of_birth, date_of_birth, job, photo');
            $this->db->from('users');
            $this->db->where('username', $username);
            $query = $this->db->get();
            
            return $query->result();

        }
        
    }