<?php
    class TimelineModel extends CI_Model {

        public function __construct() {
            // Call the Model constructor
            parent::__construct();
        }

        public function insert_new_status($profile_id) {
            $data = array(
                        'content' => html_escape($this->input->post('status', TRUE)),
                        'date' => date("Y-m-d H:i:s", time()),
                        'profile_id' => $profile_id
                    );

            $this->db->insert('statuses', $data);
            
        }

        public function get_status_by_id($id) {
            $this->db->select('*')->from('statuses')->where('id', $id);

            $query = $this->db->get();

            return $query->result();
        }

        public function delete_status($status_id) {
            $this->db->delete('statuses', array('id' => $status_id)); 
        }

        public function get_profile_statuses($profile_id) { 
            $this->db->from('statuses');
            $this->db->where('profile_id', $profile_id);   
            $this->db->order_by('date', 'desc');  

            $query = $this->db->get();
            return $query->result();
        }

        public function get_timeline_from($profile_id, $array) {
            //$array is an array of stdObject with the object only has 1 attr, id

            $this->db
                    ->select('id, first_name, last_name, photo')
                    ->from('users');

            $subquery = $this->db->_compile_select();

            $this->db->_reset_select(); 

            $this->db->select('statuses.id as id, users.id as profile_id, users.first_name, users.last_name, users.photo, content, date, type, attachment')->from('statuses');

            $this->db->where('profile_id', $profile_id);
            foreach ($array as $user) {
                $this->db->or_where('profile_id', $user->id);
            }

            $this->db->join("($subquery) users","users.id = statuses.profile_id");

            $this->db->order_by("statuses.id", "DESC");

            $query = $this->db->get();

            return $query->result();
        }
        
    }