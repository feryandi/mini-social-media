<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct() {
		parent::__construct();
		
	    $this->load->model('UserModel');

		$session_data = $this->session->userdata('login');
		if ( $this->UserModel->is_admin($session_data['id']) ) {
            redirect('control', 'refresh');	    			
		}
	}

	public function index() {
        if($session_data = $this->session->userdata('login')) {

        	if ( $query = $this->input->get('q') ) {
		        $this->load->model('SearchModel');

		        $data = $this->SearchModel->user($query);
		        
				$passer['data'][] = $data;
		        $this->load->view('/layout/header.php');
				$this->load->view('/search/index.php', $passer);
		        $this->load->view('/layout/footer.php');
		    } else {
		    	redirect('home', 'refresh');	    	
		    }
	    
	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	    	
	    }
	}
}
