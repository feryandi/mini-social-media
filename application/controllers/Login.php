<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();

	    $this->load->model('UserModel');

		if ( $session_data = $this->session->userdata('login') ) {
			if ( $this->UserModel->is_admin($session_data['id']) ) {
	            redirect('control', 'refresh');	    			
			} else {
	            redirect('home', 'refresh');	    				
			}
		}
	}

	public function index() {
		$this->load->view('/login/index.php');
	}

	public function auth() {		

		$this->load->database();

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ( ($this->form_validation->run()) ) {

	        $this->load->model('AuthModel');

	        $data = $this->AuthModel->user_login();

			if ( ( $data != NULL ) ) {
				/* Session Identifier */
				$session = array(
								'id' => $data[0]->id,
								'first_name' => $data[0]->first_name,
								'last_name' => $data[0]->last_name,
								'email' => $data[0]->email,
								'access' => $data[0]->access,
								'is_admin' => $data[0]->is_admin
							);

				$this->session->set_userdata('login', $session);

				if ( $session['is_admin'] ) {
					redirect('control', 'refresh');
				} else {
				    redirect('home', 'refresh');
				}

			} else {

				$alert = array();
				$alert['alert'] = "Wrong username or password";
				$this->load->view('login/index', $alert);

			}
		
		} else {

			$this->load->view('login/index');

		}
	}

}
