<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
	    $this->load->model('UserModel');
	}

	public function index() {

		$this->load->view('/layout/header.php');

		$session_data = $this->session->userdata('login');
		if( $this->session->userdata('login') && ( !$this->UserModel->is_admin($session_data['id']) ) ) {

	        $this->load->model('UserModel');
	        $this->load->model('TimelineModel');
	        $this->load->model('FriendModel');

	        $data = $this->UserModel->user_profile($session_data['id']);
	        $data[0]->session_userid = $session_data['id'];

	        $friends = $this->FriendModel->get_all_friend_id($session_data['id']);
	        $data[0]->timeline = $this->TimelineModel->get_timeline_from($session_data['id'], $friends);

			$this->load->view('/home/index.php', $data[0]);
		
	   	} else {
	    	//If no session, redirect to login page
	    	redirect('login', 'refresh');
	   	}

		$this->load->view('/layout/footer.php');
	}

	public function logout() {
		$this->session->unset_userdata('login');
		session_destroy();
		redirect('/', 'refresh');
	}

	public function add_status() {
		$this->load->database();

		$this->form_validation->set_rules('status', 'Status', 'required');

		if ( $session_data = $this->session->userdata('login') ) {

			if ( ($this->form_validation->run()) ) {

		       	$this->load->model('TimelineModel');

				$this->TimelineModel->insert_new_status($session_data['id']);

				redirect('home', 'refresh');
				
			
			} else {

				redirect('home', 'refresh');

			}
		}
	}
}
