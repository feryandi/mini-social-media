<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function index() {
		$this->load->view('/register/index.php');
	}

	public function send() {		
		$this->load->database();

		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|min_length[5]|max_length[25]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
		$this->form_validation->set_rules('first_name', 'First name', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('last_name', 'Last name', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('relationship', 'Relationship', 'required|min_length[1]|max_length[30]');

		if ( ($this->form_validation->run()) ) {

	       $this->load->model('UserModel');

	        $data = array();
	        $data['username'] = $this->UserModel->insert_new_user();

			if ($data['username'] == NULL)
			{
			    redirect('register', 'refresh');
			} else {
			    redirect('login', 'refresh');
			}
		
		} else {

			$this->load->view('register/index');

		}
	}
}
