<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller {

	public function index() {
        $this->load->view('/api.php');
	}

	public function user_get_info() {
        $username = $this->input->post('username');

		$this->load->database();

        $this->load->model('UserModel');

        if ( isset($username) ) {
        	$data = $this->UserModel->api_get_user_profile_by_username($username);
        	$data = json_encode($data[0]);
	        echo($data);

        } else {
        	echo('{"errors":[{"message":"Sorry, that page does not exist","code":34}]}');
        }

	}

}
