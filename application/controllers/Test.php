<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function index() {

		$this->load->view('/layout/header.php');
			
		$this->load->view('/test/index.php');

		$this->load->view('/layout/footer.php');
	}
}
