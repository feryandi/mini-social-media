<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct() {
		parent::__construct();
		
	    $this->load->model('UserModel');

		$session_data = $this->session->userdata('login');
		if ( $this->UserModel->is_admin($session_data['id']) ) {
            redirect('control', 'refresh');	    			
		}
	}

	public function index() {
        if($session_data = $this->session->userdata('login')) {

	        $this->load->model('UserModel');
	        $this->load->model('TimelineModel');
	        $this->load->model('FriendModel');

	        $data = $this->UserModel->user_profile($session_data['id']);

	        $data[0]->session_userid = $session_data['id'];
	        $data[0]->statuses = $this->TimelineModel->get_profile_statuses($session_data['id']);

	        $this->load->view('/layout/header.php');
			$this->load->view('/profile/index.php', $data[0]);
	        $this->load->view('/layout/footer.php');
	    
	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	    	
	    }
	}

	public function show($id) {
        if( ($session_data = $this->session->userdata('login')) && ($session_data['id'] != $id) ) {

	        $this->load->model('UserModel');
	        $this->load->model('TimelineModel');
	        $this->load->model('FriendModel');

	        $data = $this->UserModel->user_profile($id);

	        $data[0]->session_userid = $session_data['id'];
	        $data[0]->statuses = $this->TimelineModel->get_profile_statuses($id);
	        
	        $this->load->view('/layout/header.php');
			$this->load->view('/profile/index.php', $data[0]);
	        $this->load->view('/layout/footer.php');
	    
	    } else {
            //If no session, redirect to login page
            redirect('profile');	    	
	    }
	}

	public function edit() {

        if($session_data = $this->session->userdata('login')) {

	        $this->load->model('UserModel');

	        $data = $this->UserModel->user_profile($session_data['id']);
	        
	        $this->load->view('/layout/header.php');
			$this->load->view('/profile/edit.php', $data[0]);
	        $this->load->view('/layout/footer.php');	
	    
	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	    	
	    }

	}

	public function edit_picture() {

        if($session_data = $this->session->userdata('login')) {
	        
	        $this->load->model('UserModel');
	        $data = $this->UserModel->user_profile($session_data['id']);

	        $this->load->view('/layout/header.php');
			$this->load->view('/profile/edit_picture.php', $data[0]);
	        $this->load->view('/layout/footer.php');	
	    
	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	    	
	    }

	}

	public function send_edit() {

		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('relationship', 'Password', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('birth_place', 'Birth place', 'min_length[1]|max_length[50]');
		$this->form_validation->set_rules('birth_date', 'Birth Date', 'greater_than[1]|less_than[31]');
		$this->form_validation->set_rules('birth_month', 'Birth Month', 'greater_than[1]|less_than[12]');
		$this->form_validation->set_rules('birth_year', 'Birth Year', 'greater_than[1900]');
		$this->form_validation->set_rules('occupancy', 'Occupancy', 'min_length[1]|max_length[30]');

        if($session_data = $this->session->userdata('login')) {
			if ( ($this->form_validation->run()) ) {

					$this->load->database();

			        $this->load->model('UserModel');
			        $data = $this->UserModel->edit_profile($session_data['id']);
		            redirect('profile', 'refresh');	  	    	

			} else {

		        $this->load->model('UserModel');

		        $data = $this->UserModel->user_profile($session_data['id']);
		        
		        $this->load->view('/layout/header.php');
				$this->load->view('/profile/edit.php', $data[0]);
		        $this->load->view('/layout/footer.php');	

			}
	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	  	    	
	    }

	}

	public function send_profile_picture() {
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '2000';
		$config['max_height']  = '2000';
		$config['encrypt_name']  = TRUE;

		$this->load->library('upload', $config);

        if($session_data = $this->session->userdata('login')) {

			if ( !($this->upload->do_upload('picture')) ) {
				echo $this->upload->display_errors('<p>', '</p>');
				redirect('profile', 'refresh');

			} else {

				$data = array('upload_data' => $this->upload->data());

				$this->load->database();

		        $this->load->model('UserModel');
		        $this->UserModel->edit_profile_picture($session_data['id'], $data['upload_data']);

		        redirect('profile', 'refresh');	  	 
			}   	

	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	  	    	
	    }

	}

	public function add_friend() {

		$id = $this->input->post('id', TRUE);
		$key = $this->input->post('csrf_key', TRUE);

		if ( md5($id . $this->config->config['encryption_key']) == $key ) {

	        if($session_data = $this->session->userdata('login')) {

				$this->load->database();

		        $this->load->model('FriendModel');
		        $this->FriendModel->add_friend($session_data['id'], $id);

		        redirect('profile/show/' . $id, 'refresh');	  	 				

		    } else {
	            //If no session, redirect to login page
	            redirect('login', 'refresh');	  	    	
		    }

		} else {

	        redirect('home', 'refresh');

		}

	}

	public function delete_status($id) {
        if($session_data = $this->session->userdata('login')) {

			$this->load->database();

	        $this->load->model('TimelineModel');
	        $status = $this->TimelineModel->get_status_by_id($id);

	        if ( $status[0]->profile_id == $session_data['id'] ) {
	        	$this->TimelineModel->delete_status($id);
	        	redirect('profile', 'refresh');	  
	        } else {
	        	redirect('home', 'refresh');	  
	        }	 				

	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	  	    	
	    }
	}

}
