<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Control extends CI_Controller {

	public function index($id) {
        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) ) {

	        $this->load->model('UserModel');
            $cons = 5; //Five users per page

	        if ( !isset($id) ) {
		        $data = $this->UserModel->get_all_users();
			} else {
		        $data = $this->UserModel->get_some_users($id, $cons);				
			}
	        
			$passer['data'][] = $data;
			$passer['data']['page_id'] = $id;
			$passer['data']['total_users'] = $this->UserModel->get_total_users();
			$passer['data']['user_per_page'] = $cons;

	        $this->load->view('/layout/admin/header.php');
			$this->load->view('/control/index.php', $passer);
	        $this->load->view('/layout/admin/footer.php');
	    
	    } else {
            //If not an admin, redirect to login page
            redirect('home', 'refresh');	    	
	    }
	}

	public function delete_user($id) {
        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) ) {

	        $this->load->model('UserModel');

	        $this->UserModel->delete_user($id);
	        
            redirect('control', 'refresh');	    
	    
	    } else {
            //If not an admin, redirect to login page
            redirect('home', 'refresh');	    	
	    }		
	}

	public function edit_user($id) {
        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) ) {

	        $this->load->model('UserModel');
	        $data = $this->UserModel->user_profile($id);
	        
	        $this->load->view('/layout/admin/header.php');
			$this->load->view('/control/edit_profile.php', $data[0]);
	        $this->load->view('/layout/admin/footer.php');	

	    } else {
            //If not an admin, redirect to login page
            redirect('home', 'refresh');	    	
	    }		
	}

	public function send_edit($id) {

		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('relationship', 'Relationship', 'required|min_length[1]|max_length[30]');
		$this->form_validation->set_rules('birth_place', 'Birth place', 'min_length[1]|max_length[50]');
		$this->form_validation->set_rules('birth_date', 'Birth Date', 'greater_than[1]|less_than[31]');
		$this->form_validation->set_rules('birth_month', 'Birth Month', 'greater_than[1]|less_than[12]');
		$this->form_validation->set_rules('birth_year', 'Birth Year', 'greater_than[1900]');
		$this->form_validation->set_rules('occupancy', 'Occupancy', 'min_length[1]|max_length[30]');

	        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) ) {
				
				if ( ($this->form_validation->run()) ) {

					$this->load->database();

			        $this->load->model('UserModel');
			        $data = $this->UserModel->edit_profile($id);
		            redirect('/control', 'refresh');

				} else {

			        $this->load->model('UserModel');
			        $data = $this->UserModel->user_profile($id);
			        
			        $this->load->view('/layout/admin/header.php');
					$this->load->view('/control/edit_profile.php', $data[0]);
			        $this->load->view('/layout/admin/footer.php');

				}	  	    	

		    } else {
	            //If not an admin, redirect to login page
	            redirect('home', 'refresh');	  	    	
		    }

	}

	public function add_user() {
        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) ) {
	        
	        $this->load->view('/layout/admin/header.php');
			$this->load->view('/control/add_user.php');
	        $this->load->view('/layout/admin/footer.php');	

	    } else {
            //If not an admin, redirect to login page
            redirect('home', 'refresh');	    	
	    }		
	}

	public function new_user() {
        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) ) {		
			$this->load->database();

			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|min_length[5]|max_length[25]');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
			$this->form_validation->set_rules('first_name', 'First name', 'required|min_length[1]|max_length[30]');
			$this->form_validation->set_rules('last_name', 'Last name', 'required|min_length[1]|max_length[30]');
			$this->form_validation->set_rules('relationship', 'Relationship', 'required|min_length[1]|max_length[30]');

			if ( ($this->form_validation->run()) ) {
		        $this->load->model('UserModel');

		        $data = array();
		        $data['username'] = $this->UserModel->insert_new_user();

				if ($data['username'] == NULL)
				{
				    redirect('/control/add_user', 'refresh');
				} else {
				     redirect('control', 'refresh');
				}
			} else {

		        $this->load->view('/layout/admin/header.php');
				$this->load->view('/control/add_user.php');
		        $this->load->view('/layout/admin/footer.php');	

			}

	    } else {
            //If not an admin, redirect to login page
            redirect('home', 'refresh');	    	
	    }		
	}

	public function edit_picture($id) {

        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) ) {
	        
	        $this->load->model('UserModel');
	        $data = $this->UserModel->user_profile($id);
	        
	        $this->load->view('/layout/admin/header.php');
			$this->load->view('/control/edit_picture.php', $data[0]);
	        $this->load->view('/layout/admin/footer.php');	
	    
	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	    	
	    }

	}

	public function send_profile_picture($id) {
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '2000';
		$config['max_height']  = '2000';
		$config['encrypt_name']  = TRUE;

		$this->load->library('upload', $config);

        if ( ($session_data = $this->session->userdata('login')) && ($session_data['is_admin']) )  {

			if ( !($this->upload->do_upload('picture')) ) {
				echo $this->upload->display_errors('<p>', '</p>');
				//redirect('home', 'refresh');

			} else {

				$data = array('upload_data' => $this->upload->data());

				$this->load->database();

		        $this->load->model('UserModel');
		        $this->UserModel->edit_profile_picture($id, $data['upload_data']);

		        redirect('control', 'refresh');	  	 
			}   	

	    } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');	  	    	
	    }

	}

}
