window.onload = function() {
   	var d = new Date();
	var n = d.getHours();

	if ( n >= 5 && n < 11 ) {
		document.getElementById("greetings").innerHTML = "Good Morning";
		document.getElementById("greetings-illust").src = "img/illust_morning.png";
	} else if ( n >= 11 && n < 15 ) {
		document.getElementById("greetings").innerHTML = "Good Afternoon";
		document.getElementById("greetings-illust").src = "img/illust_noon.png";
	} else if ( n >= 15 && n < 17 ) {
		document.getElementById("greetings").innerHTML = "Good Afternoon";
		document.getElementById("greetings-illust").src = "img/illust_noon.png";
	} else if ( n >= 17 && n < 19  ) {
		document.getElementById("greetings").innerHTML = "Good Evening";
		document.getElementById("greetings-illust").src = "img/illust_evening.png";
	} else if ( n >= 19 || n < 5 ) {
		document.getElementById("greetings").innerHTML = "Good Night";
		document.getElementById("greetings-illust").src = "img/illust_night.png";
	}
};

